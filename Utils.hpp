#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>

/*
  La class Carte doit inclure "Utils.hpp" pour utiliser CARTE_MAX_X et CARTE_MAX_Y, et "Utils.hpp" doit inclure "Classe.hpp" pour la fonction afficherCarte.
  Pour éviter cette boucle infinie, on déclare ici une classe Carte vide, et le fichier "Classe.hpp" sera inclu dans le fichier ".cpp" de Utils.
*/
class Carte;

const int CARTE_MAX_X=25;
const int CARTE_MAX_Y=15;

// Afficher Carte
void afficherCarte(Carte& carte);

// Remet à zéro l'affichage du terminal
void clearScreen();

#endif
