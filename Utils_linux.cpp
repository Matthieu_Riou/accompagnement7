#include "Utils.hpp"
#include "Carte.hpp"
#include <iostream>
#include <vector>
#include <sstream>

std::string FormatText(std::string text, std::vector<int> v)
{
    std::stringstream stream;

    for(int f : v)
    {
        stream << "\033[" << f << "m";
    }

    stream << text << "\033[0m";

    std::string formatted = stream.str();

    return formatted;
}

void afficherCarte(Carte& carte)
{
    std::string s;

    // Top hline
    for(int i = 0; i < CARTE_MAX_X*3+2; i++)
    {
        s += "=";
    }

    s+= "\n";

    for(int j = 0; j < CARTE_MAX_Y; j++)
    {
        // Left vline
        s += "|";

        // Grid
        for(int i = 0; i < CARTE_MAX_X; i++)
        {
            Position pos(i,j);
            bool case_vide = carte.getCase(pos) == nullptr;

            if(carte.getPosition().x == i && carte.getPosition().y ==j)
            {
                s += FormatText(" P ", {100});
            }
            else if(case_vide)
            {
                s += "   ";
            }
            else
            {
                s += FormatText(carte.getCase(pos)->toString(), {100});
            }

        }

        // Left vline
        s += "|\n";
    }


    // Bottom hline
    for(int i = 0; i < CARTE_MAX_X*3+2; i++)
    {
        s += "=";
    }
    s += "\n";


    std::cout << s << std::endl;
}

void clearScreen()
{
    system("clear");
}
