#include "Utils.hpp"
#include "Carte.hpp"
#include <iostream>
#include <windows.h>

void afficherCarte(Carte& carte)
{
    // Top hline
    for(int i = 0; i < CARTE_MAX_X*3+2; i++)
    {
        std::cout << "=";
    }

    std::cout << std::endl;

    for(int j = 0; j < CARTE_MAX_Y; j++)
    {
        // Left vline
        std::cout << "|";

        // Grid
        for(int i = 0; i < CARTE_MAX_X; i++)
        {
            Position pos(i,j);
            bool case_vide = carte.getCase(pos) == nullptr;

            if(carte.getPosition().x == i && carte.getPosition().y ==j)
            {
                SetConsoleTextAttribute (GetStdHandle(STD_OUTPUT_HANDLE), 143);
                std::cout << " P ";
                SetConsoleTextAttribute (GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
            }
            else if(case_vide)
            {
                std::cout << "   ";
            }
            else
            {
                SetConsoleTextAttribute (GetStdHandle(STD_OUTPUT_HANDLE), 143);
                std::cout << carte.getCase(pos)->toString();
                SetConsoleTextAttribute (GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);
            }

        }

        // Left vline
        std::cout << "|" << std::endl;
    }


    // Bottom hline
    for(int i = 0; i < CARTE_MAX_X*3+2; i++)
    {
        std::cout << "=";
    }
    std::cout << std::endl;
}

void clearScreen()
{
    system("CLS");
}
